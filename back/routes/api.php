<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookAdController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//BOOK AD ROUTES{

Route::get('book_ads',[BookAdController::class,'index']);

Route::get('book_ads/{id}',[BookAdController::class,'show']);

Route::post('book_ads',[BookAdController::class,'create']);

Route::put('book_ads/{id}',[BookAdController::class,'update']);

Route::delete('book_ads/{id}',[BookAdController::class,'destroy']);

//}

//USER ROUTES{

    Route::get('user',[UserController::class,'index']);

    Route::get('user/{id}',[UserController::class,'show']);

    Route::post('user',[UserController::class,'create']);

    Route::put('user/{id}',[UserController::class,'update']);

    Route::delete('user/{id}',[UserController::class,'destroy']);
    
//}

//COMMENT ROUTES{

    Route::get('comment',[CommentController::class,'index']);

    Route::get('comment/{id}',[CommentController::class,'show']);

    Route::post('comment',[CommentController::class,'create']);

    Route::put('comment/{id}',[CommentController::class,'update']);

    Route::delete('comment/{id}',[CommentController::class,'destroy']);
    
//}

