<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BookAd extends Model
{
    public function createBook(Request $request){
        $this->name = $request->name;
        $this->author = $request->author;
        $this->price = $request->price;
        $this->score = $request->score;
        $this->resume = $request->resume;
        $this->image = $request->image;
        $this->save();
    }

//     public function updateBook(Request $request, $id){
//         if($request->name){
//             $this->name = $request->name;
//         }
//         if($request->author){
//             $this->author = $request->author;
//         }
//         if($request->price){
//             $this->price = $request->price;
//         }
//         if($request->score){
//             $this->score = $request->score;
//         }
//         if($request->resume){
//             $this->resume = $request->resume;
//         }
//         if($request->image){
//             $this->image = $request->image;
//         }
//     }
// }
}