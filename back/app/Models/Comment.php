<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BookAd extends Model
{
    public function createComment(Request $request){
        $this->text = $request->text;
        $this->score = $request->score;

        $this->save();
    }
}