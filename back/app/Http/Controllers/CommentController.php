<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function create(Request $request){
        $comment = new Comment;
        $comment->text = $request->text;
        $comment->score = $request->score;
        $comment->save();
        return response()->json(['comment'=> $comment], 200);
    }

    public function index(){
        $comments = Comment::all();
        return response()->json(['comment' => $comment],200);
    }

    public function show($id){
        $comments = Comment::find($id);
        return response()->json(['comment'=> $comments], 200);
    }

    public function update (Request $request, $id){
        $comment = Comment::find($id);
        if($request->text){
            $comment->text = $request->text;
        }
        if($request->score){
            $comment->score = $request->score;
        }
        return response()->json(['comment' => $comment], 200);
    }

    public function destroy($id){
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json(['Comentário deletado com sucesso'],200);
    }
}
