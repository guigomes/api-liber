<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function create(Request $request){
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->score = $request->score;
        $user->adress = $request->adress;
        $user->password = $request->password;
        $user->save();
        return response()->json(['user'=> $user], 200);
    }

    public function index(){
        $users = User::all();
        return response()->json(['user' => $users],200);
    }

    public function show($id){
        $user = User::find($id);
        return response()->json(['user'=> $user], 200);
    }

    public function update (Request $request, $id){
        $user = User::find($id);
        if($request->name){
            $user->name = $request->name;
        }
        if($request->email){
            $user->email = $request->email;
        }
        if($request->phone){
            $user->phone = $request->phone;
        }
        if($request->score){
            $user->score = $request->score;
        }
        if($request->adress){
            $user->adress = $request->adress;
        }
        if($request->password){
            $user->password = $request->password;
        }
        return response()->json(['user' => $user], 200);
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        return response()->json(['Usuário deletado com sucesso'],200);
    }
}
