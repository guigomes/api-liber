<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BookAd;

class BookAdController extends Controller
{
    public function create(Request $request){
        $book_ad = new BookAd;
        $book_ad->name = $request->name;
        $book_ad->author = $request->author;
        $book_ad->price = $request->price;
        $book_ad->score = $request->score;
        $book_ad->resume = $request->resume;
        $book_ad->image = $request->image;
        $book_ad->user_id = $request->user_id;
        $book_ad->save();
        return response()->json(['book_ad'=> $book_ad], 200);
    }

    public function index(){
        $book_ads = BookAd::all();
        return response()->json(['book_ad' => $book_ads],200);
    }

    public function show($id){
        $book_ad = BookAd::find($id);
        return response()->json(['book_ad'=> $book_ad], 200);
    }

    public function update (Request $request, $id){
        $book_ad = BookAd::find($id);
        if($request->name){
            $book_ad->name = $request->name;
        }
        if($request->author){
            $book_ad->author = $request->author;
        }
        if($request->price){
            $book_ad->price = $request->price;
        }
        if($request->score){
            $book_ad->score = $request->score;
        }
        if($request->resume){
            $book_ad->resume = $request->resume;
        }
        if($request->image){
            $book_ad->image = $request->image;
        }
        return response()->json(['book_ad' => $book_ad], 200);
    }

    public function destroy($id){
        $book_ad = BookAd::find($id);
        $book_ad->delete();
        return response()->json(['Livro deletado com sucesso'],200);
    }
}
